// Our task:
// Creat a todo list with the following functions:

// 1. showTodos
// Wil return an array of all todos

// 2. addTodos(todo)
// Will add a new todo in our array of todos

// 3. updateTodo()
// Given an index, will update the todo

// 4. deleteTodo()
// Given an index, will delete a todo

// Step 1 Create an empty todos array

const todos = [];

// Step 2 Create a function that will add a new todo

function addTodo(task){
	//Things to consider:
	//a. What is the data type of the todo data? String? Object?
	//b. What are the parameters that this function needs?

	const newTodo = task;

	// push the newTodo into our todos array

	todos.push(newTodo);
	return "Successfully added " + newTodo;
}

function showTodos(){
	return todos;
}

function updateTodo(index, updatedTask){
	//Things to consider:
	//a. When updating a data, we need something to identify which data we want to update. In this case, we will use the index.
	//b. We need the updated value
	todos[index] = updatedTask;
	return "Successfully updated task!"
}

function deleteTodo(index){
	//Things to consider:
	//a. When deleting a data, we need something to identify which data we want to delete.
	todos.splice(index, 1);
	return "Successfully deleted task!"
}