// ARRAY - is a collection of related data denoted by [].
// ex.
const fruits = ['Apple', 'Banana', 'Kiwi'];

const ages = [1,2,3,4];

const students = [ 
	{name: "Brandon", age: 11}, 
	{name: "Brenda", age: 12},
	{name: "Celmer", age: 15},
	{name: "Archie", age: 16},
]
// It is recommended to use plural form when naming an array, and singular when naming an object

// HOW TO ACCESS ARRAYS
// >>bracket notation<<
// We need to use bracket notation and the index of the item we want to access.
// Index - position of data relative to the position of the first data
// Index - starts at 0

// ACCESS
// to access Apple, we enter "fruits[0];"
// to access Banana, we enter "fruits[1];"
// to access an object, we enter "students[0/1/2/3];"
// to access an object within an array, we enter "students[n].object"

// CHANGE
// to change the age we enter "students[n].age = (new age)"

// ADD
// to add a data in an array, we will use the method " .push(); " 
// this method will return the length of the array
	// length - # of data inside the array
// ex. array.push(dataWeWantToAdd);

// to add a fruit inside the fruits array:
// fruits.push['Blueberry'];
// this data will be added at the end of the array

// to add a data at the beginning of an array, we use the method " .unshift(); "
// array.unshift(asdasd);
// this will return the new length of the array

// UPDATE
// to update a data, access the data first via bracket notation and then re-assign a value.

// students[2] = {name: "Asdasd", age: 19}

// students[n].property = newValue;
// ex. students[1].age = 20;
// this updates Brenda's age from 12 to 20.

// DELETE
// to delete a data at the end of an array, we use the method " .pop(); "
// array.pop();
// this will return the deleted data

// to delete a data at the start of an array, we use the method " .shift(); "
// array.shift();


// PUSH --- add / end
// POP --- delete / end
// SHIFT --- delete / start
// UNSHIFT --- add / start

// .splice(arg1, arg2, arg3, argN); 
// - takes are least 2 arguments
// arg1 - starting index
// arg2 - number of items you want to delete
// arg3, argN - the items you want to add

// fruits.splice(1,1);
// this will delete Banana and the console will return an array of deleted items

// fruits.splice(1,0,"Orange");
// this will insert Orange to index 1 and will return an empty array of deleted items
// This can be used to add data in the MIDDLE of the array

// fruits.splice(1,1,"Orange");
// this will replace Banana with Orange and will return an array with Banana which is the deleted data
