const pila = [
	"Alfred",
	"Argie",
	"Arvin",
	"Christopher",
	"Dominic",
	"Donna",
	"Earl",
	"Janamir",
	"Jeanne",
	"Jethro",
	"John",
	"Julie",
	"Karl",
	"Kaycee",
	"Keisha",
	"Louie",
	"Trina",
	"Mark Jay",
	"Michael",
	"Nizar",
	"Paul",
	"Regine",
	"Rolando",
	"Tammy"
];

function showAll(){
	return pila;
};

function addQueue(name){
	const addName = name;
	pila.push(addName);
	return "Successfully added " + addName;
};

function deQueue(){
	pila.shift();
	return "First person removed"
}

function expedite(index){
	pila.splice(index, 1);
	return "Person " + index + " removed";
}

function reverseDequeue(){
	pila.pop();
	return "Last person removed"
}

function addVIP(name){
	const addName = name;
	pila.unshift(addName);
	return "Successfully added " + addName + " (VIP)";
}

function updateName(index, newName){
	pila[index] = newName;
	return "Successfully replaced person " + index;
}
